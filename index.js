const ZipToMarkdown = require('./src/ZipToMarkdown');
const [, , source, destination] = process.argv;

if (source) {
    const translator = new ZipToMarkdown(source, destination);
} else {
    console.error('Usage: node index.js <source URL> (<destination path>)');
}
