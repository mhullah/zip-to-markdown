const extract = require('extract-zip');
const fs = require('fs');
const path = require('path');

const download = require('./download');
const guid = require('./guid');
const template = require('./template');

const FILENAMES = ['title.txt', 'image.png', 'image.txt', 'message.txt'];

/**
 * A simple class for translating pre-structured ZIP files into Markdown.
 */
module.exports = class ZipToMarkdown {
    /**
     * Create an instance of ZipToMarkdown.
     * @param {string} url The url (file/HTTP/HTTPS) to the source ZIP file.
     */
    constructor(source, destination) {
        this.source = source;
        this.destination = path.resolve(destination || `./${guid()}`);

        if (!fs.existsSync(this.destination)) {
            // Create the destination path.
            fs.mkdirSync(this.destination);
        }

        this.exec();
    }

    /**
     * Do everything necessary to translate a ZIP file into Markdown.
     * @returns {Promise}
     */
    exec() {
        return this.download()
            .then(file => this.extract(file))
            .then(() => this.verify())
            .then(files => this.translate(files))
            .then(() => console.log(`Result: <${path.join(this.destination, 'output.md')}>`))
            .catch(error => console.error(error.message));
    }

    /**
     * Download the ZIP file to the working path.
     * @returns {Promise<WriteStream>}
     */
    download() {
        return download(this.source, path.join(this.destination, 'input.zip'));
    }

    /**
     * Extract the ZIP file to the working path.
     * @param {WriteStream} file
     * @returns {Promise}
     */
    extract(file) {
        return new Promise((resolve, reject) => {
            extract(file.path, { dir: this.destination }, (error) => {
                if (error) {
                    reject(Error(`The source file could not be extracted.`));
                } else {
                    resolve();
                }
            });
        });
    }

    /**
     * Read, verify and return the expected files.
     * @returns {Promise[]}
     */
    verify(file) {
        return Promise.all(FILENAMES.map((filename) => {
            const file = path.join(this.destination, filename);

            if (!fs.existsSync(file)){
                throw Error(`${filename} was not found.`);
            }

            return fs.readFileSync(file);
        }));
    }

    /**
     * Translate the extracted files into Markdown.
     */
    translate([title, image, caption, message]) {
        return new Promise((resolve, reject) => {
            const file = fs.createWriteStream(path.join(this.destination, 'output.md'));

            file.write(template({
                title: title.toString().trim(),
                image: path.join(this.destination, 'image.png'),
                caption: caption.toString().trim(),
                message: message.toString().trim(),
            }));

            file.close(resolve);
        });
    }
}
