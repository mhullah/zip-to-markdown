/**
 * Creates and returns a pseudo-unique GUID.
 * @returns {string}
 */
module.exports = function guid() {
    return Math.random().toString(36).substring(2, 8);
}
