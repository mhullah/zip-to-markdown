const fs = require('fs');
const http = require('https');

/**
 * Downloads the source URL to the destination file.
 * @param {string} source
 * @param {string} destination
 * @returns {Promise<WriteStream>}
 */
module.exports = function download(source, destination) {
    return new Promise((resolve, reject) => {
        const file = fs.createWriteStream(destination);

        file.on('finish', () => {
            file.close(() => resolve(file));
        });

        // Handle a local file.
        if (fs.existsSync(source)) {
            fs.createReadStream(source).pipe(file);
            return;
        }

        // Handle a remote file.
        http.get(source, (response) => {
            if (response.statusCode !== 200) {
                reject(Error(`<${source}> could not be downloaded: ${response.statusCode} - ${response.statusMessage}`));
            } else {
                response.pipe(file);
            }
        }).on('error', (error) => {
            fs.unlink(file.path, () => { });
            reject(Error(`<${source}> could not be downloaded: ${error.message}`));
        });
    });
};
