/**
 * Generates and returns a Markdown-formatted string.
 */
module.exports = function( {title, image, caption, message} ){
    return `# ${title}\n\n![${caption}](${image})\n\n${message}\n`;
}
