# ZIP to Markdown

This is a simple app that takes an input URL, extracts it, and translates the contents into a Markdown file.

## Requirements

- An environment with Node.js and npm available.

# Instructions

1. Clone the repository:

    ```
    git clone https://gitlab.com/mhullah/zip-to-markdown.git
    cd zip-to-markdown
    ```

1. Install any dependency packages:

    ```
    npm i
    ```

1. Run the included test cases:

    ```
    npm start ./tests/one.zip one
    npm start ./tests/two.zip two
    npm start ./tests/three.zip three
    ```

1. If the test was successful, the app will provide the full path to the output file. Otherwise, the app will output any relevant error messages.

## Source URL

The source URL must be to a valid ZIP file containing the following files:

- title.txt
- image.png
- image.txt
- message.txt

It is assumed that the mime type of each file matches the file extension.

## Why Node.js?

- Node.js adds a lot of overhead when compared to a similar implementation using a Bash script. This could be significant if a lot of jobs are run simultaneously.
- However, the app could be expanded upon to be a micro-service, serving HTTP requests at an API endpoint (or similar). By doing this, a single process could serve multiple requests, significantly reducing the overhead and increasing the usefulness of the app.
